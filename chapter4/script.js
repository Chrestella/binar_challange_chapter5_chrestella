//posisi awal game
var resultScore = 0;
const item = ["batu", "kertas", "gunting"];
const result = ["DRAW", "Player 1 Win", "COM Win"];

//function mulai game
function start(score){
    const compare=document.getElementById("result");
    if(resultScore==0){
        const logicCom=Math.floor(Math.random()*item.length);
        const pilihanComputer=item[logicCom];
        const getPilihanComputer = document.querySelectorAll(".computer img");
        getPilihanComputer[logicCom].style.backgroundColor="gray",
        compare.innerHTML=result[getInfo(score,pilihanComputer)],
        compare.style.color="white",
        compare.style.backgroundColor="#4C9654",
        compare.style.transform="rotate(-20deg)",
        compare.style.fontSize="3em",
        resultScore=1
    }
    else compare.innerHTML="restart game first", compare.style.color="yellow", compare.style.transform="none", compare.style.fontSize="2em"
}

//function comparison for result
function getInfo(score,compare){
    if (compare == score) return "0";
    else if (compare == "batu") return (score == "kertas") ? "1" : "2";
    else if (compare == "kertas") return (score == "gunting") ? "1" : "2";
    else if (compare == "gunting") return (score == "batu") ? "1" : "2";
}

//function refresh game
function restart(){
    window.location.reload()
}