Challenge Chapter 5
1. Membuat server berdasarkan ExpressJS
2. Mengimplementasi routing ExpressJS untuk page masing-masing dari Challenge Chapter 3 dan Chapter 4
Ditampilkan dalam localhost:5000/chapter3 dan localhost:5000/chapter4

3. Membuat API dengan Express JS dengan format data users.json
Ditampilkan dalam localhost:5000/users (menampilkan seluruh user berdasarkan nama (ascending))

4. Membuat collection di Postman API untuk akses endpoint users
Ditampilkan dalam link Postman Collection
https://www.getpostman.com/collections/3327610a20ef23b312a8