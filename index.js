//import framework express
const express = require('express')
//panggil objek express app
const app = express()
const port = 5000

//definisi routing
app.get('/', (req, res) => {
  res.send('Please input /chapter3 || /chapter4 || /users')
})

//routing chapter3
app.use('/chapter3', express.static ('./chapter3'))

//routing chapter4
app.use('/chapter4', express.static ('./chapter4'))

//routing users based on name (ascending)
//get the file from json to index.js
const fs = require('fs');
app.set('json spaces', 2);
app.get('/users', (req, res) => {
  fs.readFile('users.json', (err,data)=> {
    if (err) throw err;
    let usersData = JSON.parse(data);
      function compare(a, b) {
      const nameA = a.name.toUpperCase();
      const nameB = b.name.toUpperCase();
    
      let comparison = 0;
      if (nameA > nameB) {
        comparison = 1;
      } else if (nameA < nameB) {
        comparison = -1;
      }
      return comparison;
    }
    res.json(usersData.users.sort(compare));
  })
})

//menjalankan server app
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})